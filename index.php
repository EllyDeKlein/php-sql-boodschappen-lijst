<?php
require 'functions.php';
$query = require 'core\bootstrap.php'; 

require 'core\database\class.php';
require 'core\Request.php';
require 'controllers\GroceriesController.php'; 


$uri = trim($_SERVER['REQUEST_URI'], '/');

require Router::load('routes.php') 
->direct(Request::uri(), Request::method());

?>

