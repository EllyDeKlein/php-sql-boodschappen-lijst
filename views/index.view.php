<?php require('partials/header.php'); ?>
<body>
    <h1 id="h1voeg"> Boodschappenlijst </h1>
    <div id="container"></div>
    <table>
        <tr>
        <?php
            $headerTexts = ['Name', 'Prijs', 'Aantal', 'Totaal'];
            for($i=0;$i<count($headerTexts);$i++) {    
                echo "<th>" . $headerTexts[$i] . "</th>";   
            }
            ?>
            </tr>
            <?php  
                $total = 0;
                for($i=0;$i<count($products);$i++) {
                    //Result calculations
                    $result = $products[$i]->price * $products[$i]->number; //CLASS -> verwijzing 
                    
                    $total += $result;
                    //Table Rows
                    echo    "<tr>";
                    echo    "<td>" . $products[$i]->name  . "</td>" . 
                            "<td>" . $products[$i]->price . "</td>" . 
                            "<td>" . $products[$i]->number . "</td>" . 
                            "<td>" . $result . "</td>";
                    echo    "</tr>";     
                
            }   
        ?>
    </table>
  <?php require('views/partials/nav.php') ?>

    <div id="totaal">Totaal: <?=$total;?></div>
    <?php require('partials/footer.php'); ?>
</body>
</html>