<?php

/*$router->define([
    'groceries' => 'views/index.view.php', 
    'groceries/create' => 'views/create.view.php'
    'addgrocery' => 'controllers/add-groceries.php'
]); */

$router->get('groceries', 'views/index.view.php');
$router->get('groceries/create', 'views/create.view.php');
$router->post('addgrocery', 'controllers/add-groceries.php');

//var_dump($router->routes);