<?php

class QueryBuilder
{
protected $pdo;

public function __construct($pdo)  //Default waar de class on depends. It always needs a PDO Connection __construct is wat die CLASS default nodig heeft.
{
    $this->pdo = $pdo; 
}

public function selectAll($table, $intoClass) 
{
    $statement = $this->pdo->prepare('select * from list');
    $statement->execute();
    return $list = $statement->fetchAll(PDO::FETCH_CLASS, $intoClass);
}

public function insert($table, $parameters) {

    $sql = sprintf(                                      //sprintf declare variables to a string placeholder style
        'insert into %s (%s) values (%s)', 
        $table,                                        
        implode(', ', array_keys($parameters)),         //Plakt de parameters aan elkaar  en zo runt hij de code
        ':' . implode(', :', array_keys($parameters))
    );


    try {
        $statement = $this->pdo->prepare($sql);
        $statement->execute($parameters);
    } 
    catch (Exception $e)
    {
        die($e->getMessage());
    }

}



}

