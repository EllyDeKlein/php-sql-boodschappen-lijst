<?php
require 'functions.php';
$query = require 'core\bootstrap.php'; // Wat de return terug geeft in bootstrap.php (new Query Builder make::connection etc.)

require 'core\database\class.php';
require 'core\Request.php';
require 'controllers\GroceriesController.php'; 


$uri = trim($_SERVER['REQUEST_URI'], '/');

require Router::load('routes.php') 
//->direct($uri);
->direct(Request::uri(), Request::method());

?>

